<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/28
 * Time: 12:07
 */
// $item is from actionItemDetail?>
<?php //echo $this->context->renderPartail('_copyright'); ?>
<!--<h2>News Item Detail</h2>-->
<!--<br/>-->
<!--Title: <b>--><?php //echo $item['title'] ?><!--</b>-->
<!--<br/>-->
<!--Date: <b>--><?php //echo $item['date'] ?><!--</b>-->

Detail item with title <b><?php echo $title ?></b>
<br/><br/>
<?php if($itemFound !=null) { ?>
<table border="1">
    <?php foreach($itemFound as $key=>$value) { ?>
    <tr>
        <th><?php echo $key ?></th>
        <td><?php echo $value ?></td>
    </tr>
    <?php }?>
</table>

<br/>

url for this items is :<?php echo yii\helpers\Url::to(['news/item-detail','title' => $title]); ?>

<?php } else { ?>
    <i>No item found</i>
<?php } ?>
